import re
from os import environ
from pathlib import Path

import numpy as np
from ase import Atoms
from ase.calculators.espresso import Espresso, EspressoProfile
from ase.io import read
from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms
from phonopy.structure.symmetry import Symmetry

label = 'pwscf'

atoms = Atoms('H2', positions=[[0, 0, 0], [0, 0, 0.75]])
disp_size = 0.005

espresso_command = [
    'srun',
    '--cpu-freq=2250000',
    '--hint=nomultithread',
    '--distribution=block:block',
    '/work/e89/e89/td5g20/QuantumEspresso/Binaries/qe-7.2/bin/pw.x'
]

kpts = np.ceil(2 * np.pi / (atoms.cell.lengths() * 0.25)).astype(int)
kpts[kpts < 1] = 1
if np.all(kpts == 1):
    kpts = None

input_data = {
    'control': {
        'calculation': 'relax',
        'verbosity': 'high',
        'restart_mode': 'from_scratch',
        'nstep': 999,
        'tstress': False,
        'tprnfor': True,
        'outdir': 'pw.dir',
        'prefix': label,
        'max_seconds': 86100,
        'etot_conv_thr': 1.0e-8,
        'forc_conv_thr': 1.0e-7,
        'disk_io': 'low',
        'pseudo_dir': environ['ESPRESSO_PSEUDO'],
        'trism': False,
    },
    'system': {
        'ibrav': 0,
        'tot_charge': 0.0,
        'tot_magnetization': -10000,
        'ecutwfc': 60.0,
        'ecutrho': 480.0,
        'occupations': 'smearing',
        'degauss': 0.01,
        'smearing': 'cold',
        # 'input_dft': 'rpbe',
        'nspin': 2,
        'assume_isolated': 'none',
        'esm_bc': 'pbc',
        'vdw_corr': 'none',
    },
    'electrons': {
        'electron_maxstep': 999,
        'scf_must_converge': True,
        'conv_thr': 1.0e-12,
        'mixing_mode': 'local-TF',
        'mixing_beta': 0.6,
        'mixing_ndim': 8,
        'diagonalization': 'david',
        'diago_david_ndim': 2,
        'diago_rmm_ndim': 4,
        'diago_rmm_conv': False,
        'diago_full_acc': False,
    },
    'ions': {
        'ion_dynamics': 'bfgs',
        'upscale': 100,
        'bfgs_ndim': 6,
    },
    'cell': {
        'cell_dynamics': 'bfgs',
        'press_conv_thr': 0.1,
        'cell_dofree': 'all',
    }
}

u_elements = np.unique(atoms.get_chemical_symbols())

pseudo_path = input_data['control']['pseudo_dir']
pseudopotentials = {}
pot_files = pseudo_path.glob('*')
for pot_file in pot_files:
    el_name = re.split(r'[._\s]+', pot_file.name)[0]
    if el_name in u_elements:
        pseudopotentials[el_name] = pot_file.name

calc_profile = EspressoProfile(
    argv=espresso_command
)

calc = Espresso(
    pseudo_path=pseudo_path,
    pseudopotentials=pseudopotentials,
    input_data=input_data,
    kpts=kpts,
    profile=calc_profile
)

atoms.calc = calc

run_opt = True
if Path('espresso.pwo').exists():
    try:
        atoms = read('espresso.pwo')
        check_forces = atoms.get_forces()
        if np.any(np.abs(check_forces) < 1e-3):
            run_opt = False
    except Exception:
        pass
if run_opt:
    atoms.get_potential_energy()

input_data['control']['calculation'] = 'scf'

phonopy_atoms = PhonopyAtoms(symbols=atoms.get_chemical_symbols(),
                             cell=atoms.get_cell(),
                             scaled_positions=atoms.get_scaled_positions()
                             )

phonons = Phonopy(phonopy_atoms,
                  supercell_matrix=np.eye(3),
                  primitive_matrix=np.eye(3),
                  is_symmetry=True
                  )

symmetry = Symmetry(phonopy_atoms)
pointgroup = symmetry.get_pointgroup()
tol = symmetry.get_symmetry_tolerance()

print('symmetry detected:', pointgroup, 'with tolerance', tol, 'Å')

phonons.generate_displacements(distance=0.005)
displacements = phonons.get_supercells_with_displacements()

forces = []

for i, disp in enumerate(displacements):
    print(f'calculating displacement {i}', flush=True)
    # Create a directory for each displacement
    disp_dir = Path(f'disp_{i}')
    disp_dir.mkdir(exist_ok=True)
    # We build the ASE atoms object back from phonopyAtoms
    atoms = Atoms(disp.get_chemical_symbols(),
                  disp.get_positions(),
                  cell=disp.get_cell())
    # If supercell we have to recalculate the kpoints
    kpts = np.ceil(2 * np.pi / (atoms.cell.lengths() * 0.25)).astype(int)
    kpts[kpts < 1] = 1
    if np.all(kpts == 1):
        kpts = None

    calc = Espresso(
        pseudo_path=pseudo_path,
        kpts=kpts,
        directory=disp_dir,
        input_data=input_data,
        pseudopotentials=pseudopotentials,
        profile=calc_profile)

    atoms.calc = calc

    if (disp_dir / 'espresso.pwo').exists():
        try:
            forces.append(read(disp_dir / 'espresso.pwo').get_forces())
        except Exception:
            forces.append(atoms.get_forces())
    else:
        forces.append(atoms.get_forces())

phonons.forces = forces
phonons.produce_force_constants()
phonons.save(f'{label}_fc.yaml', settings={'force_constants': True})

phonons.run_mesh([50, 50, 50])

temperatures = np.arange(0, 1000, 10)
temperatures = np.append(temperatures, [273.15, 293.15, 298.15])
temperatures = np.sort(temperatures)

phonons.run_thermal_properties(temperatures=temperatures)

thermo_dict = phonons.get_thermal_properties_dict()

free_energy = thermo_dict['free_energy']
entropy = thermo_dict['entropy']
heat_capacity = thermo_dict['heat_capacity']

freq = phonons.get_frequencies_with_eigenvectors((0, 0, 0))[0] * 33.356

print('frequencies at Gamma (cm^-1):')
print(' '.join([f'{f:.8f}' for f in freq]))

print('       T (K)          F (eV)       S (eV)      Cv (eV/K)')
for T, F, S, cv in zip(temperatures, free_energy, entropy, heat_capacity):
    print(("%12.3f " + "%15.7f" * 3) % (T, F, S, cv))
